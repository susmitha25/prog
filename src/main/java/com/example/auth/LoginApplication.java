/**
 * Created by susmitha on 16/4/15.
 */
package com.example.auth;

import com.example.auth.dao.UserDao;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.example.auth.resources.LoginResource;

import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;
import org.skife.jdbi.v2.DBI;

public class LoginApplication extends Application<LoginConfiguration>{
    public static void main(String[] args) throws Exception {
        new LoginApplication().run(args);
    }



    public void initialize(Bootstrap<LoginConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<LoginConfiguration>());

    }

    @Override
    public void run(LoginConfiguration configuration,
                    Environment environment) {
        final DBIFactory factory = new DBIFactory();

        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final UserDao dao = jdbi.onDemand(UserDao.class);

        environment.jersey().register(new LoginResource(dao));

        environment.servlets().setSessionHandler(new SessionHandler());
    }

}
