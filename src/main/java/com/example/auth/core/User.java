package com.example.auth.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
    @JsonProperty
    public int id;

    @JsonProperty
    public String username;

    @JsonProperty
    public String password;

    public User(String username) {
        this.username = username;
    }

    public User(String username, String password){
        this.username=username;
        this.password=password;
        }


    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password) {
        this.password= password;}


    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id=id;
    }
}
