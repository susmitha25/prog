package com.example.auth.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by susmitha on 23/4/15.
 */


public interface UserDao {


    @SqlUpdate("insert into user (username,password) values (:username,:password)")
    void insert(@Bind("username") String username,@Bind("password") String password);


    @SqlQuery("select id from user where username = :username")
    int findId(@Bind("username") String username);

    @SqlQuery("select password from user where username = :username")
    String findPass(@Bind("username") String username);



}
