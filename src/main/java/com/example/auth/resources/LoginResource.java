package com.example.auth.resources;


import com.example.auth.core.User;
import com.example.auth.dao.UserDao;
import com.example.auth.utils.HashingUtil;
import com.example.auth.view.JoinView;
import com.example.auth.view.LoginView;
import com.example.auth.view.UserView;
import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;
import com.example.auth.utils.RoutingUtil;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.String;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/")
@Produces(MediaType.TEXT_HTML)

public class LoginResource {

    private UserDao userDao;
    int id;
    User user;


        public LoginResource(UserDao dao1){
            super();
            this.userDao = dao1;


        }

  @GET
    @Produces(MediaType.TEXT_HTML)
    public UserView getPerson(@Session HttpSession session) {
        RoutingUtil.authenticate(session);
        return new UserView(new User((String) session.getAttribute("username")));
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public UserView logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.invalidate();
        RoutingUtil.redirectToURI("/login");
        return new UserView(new User("",""));
    }

    @Path("logedin")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public UserView getLogedin(@Session HttpSession session) {
        RoutingUtil.authenticate(session);
        return new UserView(new User((String) session.getAttribute("username")));
    }
    @Path("logedin")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public void setLogedin(@Session HttpSession session) {
        session.setAttribute("username", "");
        user=new User("","");
        session.invalidate();
        RoutingUtil.redirectToURI("login");

    }
    @Path("login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView getLogin() {
        return new LoginView("");
    }
    @Path("login")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public LoginView postLogin(@FormParam("username") String username, @FormParam("password") String password,@Session HttpSession session) {


            int id = 0;
            user = new User(username, password);
            id = userDao.findId(username);
            String pass = userDao.findPass(username);

            if (id == 0) {
                user = new User("", "");
                return new LoginView(username, "user does not exist");
            } else if (!HashingUtil.GenerateHash(password).equals(pass)) {
                user = new User(username, "");
                return new LoginView(username, "incorrect password");
            } else {
                session.setAttribute("username", username);
                session.setAttribute("id", id);
                RoutingUtil.redirectToURI("logedin");
            }


          return new LoginView("","Please enter a valid username and password.");
       }


    @Path("signup")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinView getJoin() {
        return new JoinView("");
    }

    @Path("signup")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public View postJoin(@FormParam("username") String username, @FormParam("password")String password, @FormParam("confirmPass") String confirmPass,@Session HttpSession session) {

        user=new User(username,password);
        int id=0;
        id=userDao.findId(username);
        String pass=userDao.findPass(username);
        Pattern p = Pattern.compile("[^a-z0-9_ ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(username);
        boolean b = m.find();
        if(username.trim().equals("") || password.trim().equals("") || confirmPass.trim().equals("")){
            return new JoinView("enter all feilds!!");
        }
        else if(username.length()>30)
        {
            return new JoinView("username exceeds limit");
        }
        else if(password.length()<5){
            return new JoinView("password shoud have atleast 5 characters");
        }
        else  if(password.length()>30){
            return new JoinView("password exceeds limit");
        }
        else if(username.contains(" ")||b){
            return new JoinView("username cannot contain blanckspaces or special characters");
        }
        else if(id!=0)
        {
            return new JoinView("such a user already exists");
        }

        else if (!password.equals(confirmPass)) {
            return new JoinView("passwords dont match");
        }
        else {
            userDao.insert(username, HashingUtil.GenerateHash(password));
            session.setAttribute("username", username);
            RoutingUtil.redirectToURI("logedin");
        }
        return new JoinView("");
    }
}



