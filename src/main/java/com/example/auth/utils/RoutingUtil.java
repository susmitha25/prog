package com.example.auth.utils;

import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
/**
 * Created by susmitha on 27/4/15.
 */
public class RoutingUtil {
    public static void redirectToURI(String newURI) {
        URI uri = null;
        try {
            uri = new URI(newURI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Response responseObj = Response.seeOther(uri).build();
        throw new WebApplicationException(responseObj);
    }

   public static void authenticate(@Session HttpSession session) {
        String sessionAttribute = ((String) session.getAttribute("username"));
        if (sessionAttribute == null || sessionAttribute.isEmpty()) {
            RoutingUtil.redirectToURI("/login");
        }
    }

}
