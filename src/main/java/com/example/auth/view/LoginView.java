package com.example.auth.view;
import io.dropwizard.views.View;

/**
 * Created by susmitha on 27/4/15.
 */
public class LoginView extends View{
    private final String username;
    private final String msg;

    public LoginView(String username) {
        super("login.mustache");
        this.username = username;
        this.msg = "";
    }

    public LoginView(String username, String msg) {
        super("login.mustache");
        this.username = username;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getUsername() {
        return username;
    }
}
