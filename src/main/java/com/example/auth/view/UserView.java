package com.example.auth.view;


    import com.example.auth.core.User;
    import io.dropwizard.views.View;

public class UserView extends View {

         User user;

        public UserView(User user) {

            super("user.mustache");
            this.user=user;

        }

       public User getUser(){
           return user;
       }
    }

